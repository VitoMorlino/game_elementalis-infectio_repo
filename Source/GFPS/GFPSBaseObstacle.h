// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GFPSBaseObstacle.generated.h"

UCLASS()
class GFPS_API AGFPSBaseObstacle : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AGFPSBaseObstacle();

	/* Enable this per-instance to allow logging of debugging messages for this Obstacle */
	UPROPERTY(EditAnywhere, Category = "Obstacle|D E B U G G I N G")
	bool bLogDebugMessages;

protected:

	/** Should ApplyEffect() be executed when an actor hits or overlaps this obstacle?
	*	If true, ApplyEffect() will be executed on hit/overlap
	*	If false, nothing will happen on contact with this obstacle */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
	bool bIsActive;

private:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

/*****************************************************************************/
/*      Effect                                                               */
/*****************************************************************************/
/*                                                                           */

protected:

	/** Will ApplyEffect() be called repeatedly while an actor is overlapping this obstacle?
	*	If true, ApplyEffect() will be called repeatedly on every overlapping actor after TimeBetweenLoops
	*	If false, ApplyEffect() will only be called once when the actor first overlaps this obstacle */
	UPROPERTY(EditDefaultsOnly, Category = "Obstacle|Effect")
	bool bEffectLoops;

	/** Time (in seconds) between ApplyEffect() calls
	*	This value has no effect if bEffectLoops is false */
	UPROPERTY(EditDefaultsOnly, Category = "Obstacle|Effect")
	float TimeBetweenLoops;
	FTimerHandle TimerHandle_LoopEffect;

	/* The Sound Cue played every time ApplyEffect() is called */
	UPROPERTY(EditDefaultsOnly, Category = "Obstacle|Effect")
	class USoundCue* EffectSound;

	void ApplyEffectTo(AActor* OtherActor);

	/** Called when an actor hits or overlaps this obstacle, if bIsActive is true
	*	(To apply the effect repeatedly, set bEffectLoops to true)
	* @param OtherActor The Actor that the effect was applied to */
	UFUNCTION(BlueprintNativeEvent, Category = "Obstacle|Effect")
	void BPApplyEffect(AActor* OtherActor);
	virtual void BPApplyEffect_Implementation(AActor* OtherActor);

	UFUNCTION()
	void ApplyEffectToAllOverlaps();

/*****************************************************************************/
/*      Hit                                                                  */
/*****************************************************************************/
/*                                                                           */

protected:

	/* When this component is hit by another actor, ApplyEffect() is called on that actor */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Obstacle")
	class UStaticMeshComponent* MeshComp;

	/* Should ApplyEffect() be called when an actor hits (bumps into) this obstacle? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle|Hit")
	bool bApplyEffectOnHit;

	/** Time (in seconds) after an actor hits (bumps into) the MeshComp before
	* they're automatically removed from the array of RecentlyHitActors */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Obstacle|Hit")
	float OnHitCooldown;

	/** The Sound Cue played every time an actor hits (bumps into) this obstacle
	*	Leave this empty if no sound is desired on hit
	*	(The EffectSound will still play if it is set) */
	UPROPERTY(EditDefaultsOnly, Category = "Obstacle|Hit")
	class USoundCue* HitSound;

	/* Array of Actors that recently hit (bumped into) the MeshComp */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Obstacle|Hit")
	TArray<AActor*> RecentlyHitActors;

	UFUNCTION()
	virtual void OnMeshHit(
		UPrimitiveComponent* HitComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComponent,
		FVector NormalImpulse,
		const FHitResult& Hit);

	UFUNCTION()
	void RemoveFirstHitActor();

/*****************************************************************************/
/*      Overlap                                                              */
/*****************************************************************************/
/*                                                                           */

protected:

	/* Array of Actors currently overlapping this obstacle */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Obstacle|Effect")
	TArray<AActor*> OverlappingActors;

	UFUNCTION()
	virtual void OnBeginOverlap(AActor* OverlappedActor, AActor* OtherActor);

	UFUNCTION()
	virtual void OnEndOverlap(AActor* OverlappedActor, AActor* OtherActor);

};
