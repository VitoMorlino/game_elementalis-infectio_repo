#pragma once

#include "VitosLogMacros.h"
#include "GFPS.h"
#include "CustomTypes.generated.h"

UENUM(BlueprintType)
enum class EBotBehaviorType : uint8
{
	/* Stays at location (moves to waypoint only when given one) */
	Idle,

	/* Moves from Waypoint to Waypoint */
	Patrol,
};

UENUM(BlueprintType)
enum class EBotAggressiveness : uint8
{
	/* Chase/Attack Player on sight */
	Aggressive,

	/* Chase/Attack Player only when provoked */
	PassiveAggressive,

	/* Never Chase/Attack Player - Cannot be attacked/damaged */
	Passive,
};

/* The basic type of damage such as Melee, Magic, or Ranged */
UENUM(BlueprintType)
enum class EBasicDamageType : uint8
{
	/* Basic Melee Damage */
	Melee,

	/* Basic Magic Damage */
	Magic,

	/* Basic Ranged Damage */
	Ranged,
};

UENUM(BlueprintType)
enum class EElementalDamageType : uint8
{
	/* Elemental Fire Damage */
	Fire,

	/* Elemental Earth Damage */
	Earth,

	/* Elemental Water Damage */
	Water,

	/* Elemental Air Damage */
	Air,

	/* No Elemental Damage */
	None,
};

USTRUCT(BlueprintType)
struct FDamageResistance
{
	GENERATED_BODY()

	//Constructor
	FDamageResistance() :
		BasicResistance(0.f),
		ElementalResistance(0.f),
		BasicMeleeResistance(0.f),
		BasicMagicResistance(0.f),
		BasicRangedResistance(0.f),
		ElementalFireResistance(0.f),
		ElementalEarthResistance(0.f),
		ElementalWaterResistance(0.f),
		ElementalAirResistance(0.f)
	{}

	/** Maximum Total Resistance to any received damage (Basic + Elemental) (0.75 = 75% damage reduction)
	*	This is checked after total calculations are made and any resistance calculation
	*	result exceeding this value will be clamped to this value */
	UPROPERTY(VisibleAnywhere)
	float MaxTotalResistance = 0.75f;

	/** Maximum resistance to any received basic damage (0.5 = 50% damage reduction)
	*	This is checked after basic resistance calculations are made and any resistance calculation
	*	result exceeding this value will be clamped to this value */
	UPROPERTY(VisibleAnywhere)
	float MaxBasicResistance = 0.50f;

	/** Maximum resistance to any received elemental damage (0.5 = 50% damage reduction)
	*	This is checked after elemental resistance calculations are made and any resistance calculation
	*	result exceeding this value will be clamped to this value */
	UPROPERTY(VisibleAnywhere)
	float MaxElementalResistance = 0.50f;

	//Resistance to ALL Basic Damage Types (0.5 = 50% damage reduction)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Basic")
	float BasicResistance;
	//Resistance to Basic Melee Damage (0.5 = 50% damage reduction)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Basic")
	float BasicMeleeResistance;
	//Resistance to Basic Magic Damage (0.5 = 50% damage reduction)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Basic")
	float BasicMagicResistance;
	//Resistance to Basic Ranged Damage (0.5 = 50% damage reduction)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Basic")
	float BasicRangedResistance;

	//Resistance to ALL Elemental Damage Types (0.5 = 50% damage reduction)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Elemental")
	float ElementalResistance;
	//Resistance to Elemental Fire Damage (0.5 = 50% damage reduction)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Elemental")
	float ElementalFireResistance;
	//Resistance to Elemental Earth Damage (0.5 = 50% damage reduction)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Elemental")
	float ElementalEarthResistance;
	//Resistance to Elemental Water Damage (0.5 = 50% damage reduction)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Elemental")
	float ElementalWaterResistance;
	//Resistance to Elemental Air Damage (0.5 = 50% damage reduction)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Elemental")
	float ElementalAirResistance;

public:

	/* Returns the value of BasicResistance + Resistance to the specified Basic Damage Type */
	const float GetResistanceTo(EBasicDamageType BasicDmgType, bool bLogDebug)
	{
		//Start with the overall BasicResistance
		float Resistance = BasicResistance;

		//Then, add the specific type resistance
		switch (BasicDmgType)
		{
		case EBasicDamageType::Melee :
			Resistance += BasicMeleeResistance;
			break;

		case EBasicDamageType::Magic :
			Resistance += BasicMagicResistance;
			break;

		case EBasicDamageType::Ranged :
			Resistance += BasicRangedResistance;
			break;

		default:
			if (bLogDebug) LOGMSG(DmgResist, Error, "Unknown BasicDamageType");
			break;
		}

		//Finally, make sure the Basic Resistance doesn't exceed the MaxResistance
		if (Resistance > MaxBasicResistance)
		{
			if (bLogDebug)
			{
				LOGMSG_M(DmgResist, Warning,
					"Total Basic Resistance: [%f] exceeded Max Basic Resistance: [%f] ...Limiting Total Basic Resistance to [%f]",
					Resistance, MaxBasicResistance, MaxBasicResistance);
			}

			return MaxBasicResistance;
		}

		if (bLogDebug) LOGMSG_F(DmgResist, Log, "Total Basic Resistance:", Resistance)

		return Resistance;
	}

	/* Returns the value of ElementalResistance + Resistance to the specified Elemental Damage Type */
	const float GetResistanceTo(EElementalDamageType ElementalDmgType, bool bLogDebug)
	{
		//Return zero if there was no elemental damage
		if (ElementalDmgType == EElementalDamageType::None)
		{
			if (bLogDebug) LOGMSG(DmgResist, Log, "No Elemental Damage resisted...ElementalDamageType was None");

			return 0.f;
		}

		//Start with the overall ElementalResistance
		float Resistance = ElementalResistance;

		//Then, add the specific type resistance
		switch (ElementalDmgType)
		{
		case EElementalDamageType::Fire :
			Resistance += ElementalFireResistance;
			break;

		case EElementalDamageType::Earth :
			Resistance += ElementalEarthResistance;
			break;

		case EElementalDamageType::Water :
			Resistance += ElementalWaterResistance;
			break;

		case EElementalDamageType::Air :
			Resistance += ElementalAirResistance;
			break;

		default:
			if (bLogDebug) LOGMSG(DmgResist, Error, "Unknown ElementalDamageType");
			break;
		}

		//Finally, make sure the Elemental Resistance doesn't exceed the MaxResistance
		if (Resistance > MaxElementalResistance)
		{
			if (bLogDebug)
			{
				LOGMSG_M(DmgResist, Warning,
					"Total Elemental Resistance: [%f] exceeded Max Elemental Resistance: [%f]...Limiting Total Elemental Resistance to [%f]",
					Resistance, MaxElementalResistance, MaxElementalResistance);
			}

			return MaxElementalResistance;
		}

		if (bLogDebug) LOGMSG_F(DmgResist, Log, "Total Elemental Resistance:", Resistance);

		return Resistance;
	}

	/* Returns the total of GetResistanceTo(BasicDmgType) + GetResistanceTo(ElementalDmgType) */
	const float GetResistanceTo(EBasicDamageType BasicDmgType, EElementalDamageType ElementalDmgType, bool bLogDebug)
	{
		const float BasicResist = GetResistanceTo(BasicDmgType, bLogDebug);
		const float ElementalResist = GetResistanceTo(ElementalDmgType, bLogDebug);
		const float Resistance = BasicResist + ElementalResist;

		//Make sure the Total Resistance doesn't exceed the MaxResistance+
		if (Resistance > MaxTotalResistance)
		{
			if (bLogDebug)
			{
				LOGMSG_M(DmgResist, Warning,
					"Total Damage Resistance: [%f] exceeded Max Total Resistance: [%f]...Limiting Total Damage Resistance to [%f]",
					Resistance, MaxTotalResistance, MaxTotalResistance);
			}

			return MaxTotalResistance;
		}

		if (bLogDebug) LOGMSG_F(DmgResist, Log, "Total Damage Resistance: ", Resistance);
		
		return Resistance;
	}

};
