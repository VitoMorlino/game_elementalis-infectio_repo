// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(BaseChar, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(PlayerCon, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(PlayerChar, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(NonPlayerChar, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(NonPlayerCon, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(DmgResist, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(PickupActor, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(Obstacle, Log, All);
