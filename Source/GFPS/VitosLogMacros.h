/***************************************************************************************
*	UE_LOG and AddOnScreenDebugMessage Macros
*		Current Class, File, and Line Number!
*			Originally by Rama : Tweaked by Vito
*
*	PreProcessor commands to get
*		a. Class name
*		b. Function Name
*		c. Line number
*		d. Function Signature (including parameters)
*
*	<3  Rama
***************************************************************************************/

#pragma once

#include "Engine/GameEngine.h"

//add log categories to (projectname).h and include (projectname).h here
#include "GFPS.h"

/***********************************************************************************************************************************************/
/***** Class/Function Name and Line Number ***** Class/Function Name and Line Number ***** Class/Function Name and Line Number *****************/
/*                                                                                                                                             */

//Current Class Name + Function Name where this is called
#define STR_CUR_CLASS_FUNC			(FString(__FUNCTION__))

//Current Class where this is called
#define STR_CUR_CLASS				(FString(__FUNCTION__).Left(FString(__FUNCTION__).Find(TEXT(":"))) )

//Current Function Name where this is called
#define STR_CUR_FUNC				(FString(__FUNCTION__).Right(FString(__FUNCTION__).Len() - FString(__FUNCTION__).Find(TEXT("::")) - 2 ))

//Current Line Number in the code where this is called
#define STR_CUR_LINE				("(Line" + FString::FromInt(__LINE__) + ")")

//Current Class and Line Number where this is called
#define STR_CUR_CLASS_LINE			(STR_CUR_CLASS + STR_CUR_LINE)

//Current Class, Function, and Line Number where this is called
#define STR_CUR_CLASS_FUNC_LINE		(STR_CUR_CLASS_FUNC + STR_CUR_LINE)

//Current Function Signature where this is called
#define STR_CUR_FUNCSIG				(FString(__FUNCSIG__))


/*************************************************************************************************************/
/***** COLORS ***** COLORS ***** COLORS ***** COLORS ***** COLORS ***** COLORS ***** COLORS ***** COLORS *****/
/*                                                                                                           */

/**** Example: RGBCOLOR(100, 200, 255) */
#define RGBCOLOR(R, G, B)		FColor(R, G, B)

/**** Example: RGBACOLOR(100, 200, 255, 127) 
	A=127 would be 50% opacity, with 0 being fully transparent (invisible) and 255 being fully opaque */
#define RGBACOLOR(R, G, B, A)	FColor(R, G, B, A)

#define BLACK		FColor::Black
#define BLUE		FColor::Blue
#define CYAN		FColor::Cyan
#define EMERALD		FColor::Emerald
#define GREEN		FColor::Green
#define MAGENTA		FColor::Magenta
#define ORANGE		FColor::Orange
#define PURPLE		FColor::Purple
#define RED			FColor::Red
#define SILVER		FColor::Silver
#define TURQUIOSE	FColor::Turquoise
#define WHITE		FColor::White
#define YELLOW		FColor::Yellow


/**********************************************************************************************************************************************************************************************************************/
/***** GEngine->AddOnScreenDebugMessage ***** GEngine->AddOnScreenDebugMessage ***** GEngine->AddOnScreenDebugMessage ***** GEngine->AddOnScreenDebugMessage ***** GEngine->AddOnScreenDebugMessage *******************/
/*vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/*                                                                                                                                                                                                                    */

	/* Print a single FString to the screen
	*** Example: SCREENMSG(2.f, RED, "This is a screen message"); */
	#define SCREENMSG(Duration, Color, InString)				if (GEngine) (GEngine->AddOnScreenDebugMessage(-1, Duration, Color, *(STR_CUR_CLASS_FUNC_LINE + ": " + InString)))								

	/* Print 2 FStrings to the screen (One space is automatically added between InString1 and InString2)
	*** Example: SCREENMSG_2(2.f, RED, "This is a", "screen message"); */
	#define SCREENMSG_2(Duration, Color, InString1, InString2)	if (GEngine) (GEngine->AddOnScreenDebugMessage(-1, Duration, Color, *(STR_CUR_CLASS_FUNC_LINE + ": " + InString1 + " " + InString2)))				

	/* Print an FString and a Float to the screen (One space is automatically added between InString and InFloat)
	*** Example: SCREENMSG_F(2.f, RED, "HP:", Health); */
	#define SCREENMSG_F(Duration, Color, InString, InFloat)		if (GEngine) (GEngine->AddOnScreenDebugMessage(-1, Duration, Color, *(STR_CUR_CLASS_FUNC_LINE + ": " + InString + " " + FString::SanitizeFloat(InFloat))))

	/* Print an FString and an Integer to the screen (One space is automatically added between InString and InInteger)
	*** Example: SCREENMSG_F(2.f, RED, "HitCount:", NumOfHits); */
	#define SCREENMSG_I(Duration, Color, InString, InInteger)	if (GEngine) (GEngine->AddOnScreenDebugMessage(-1, Duration, Color, *(STR_CUR_CLASS_FUNC_LINE + ": " + InString + " " + FString::FromInt(InInteger))))


/*******************************************************************************************************************************************************************************************/
/***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG ***** UE_LOG *****/
/*vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv*/
/*                                                                                                                                                                                         */

	/* Log a single FString
	*** Example: LOGMSG(LogTemp, Warning, "Message"); */
	#define LOGMSG(LogCat, LogLevel, InString)					UE_LOG(LogCat, LogLevel, TEXT("%s: %s"), *STR_CUR_CLASS_FUNC_LINE, *FString(InString))							               

	/* Log 2 FStrings (One space is automatically added between InString1 and InString2)
	*** Example: LOGMSG_2(LogTemp, Warning, "Message Part 1...", "Message Part 2"); */
	#define LOGMSG_2(LogCat, LogLevel, InString1, InString2)	UE_LOG(LogCat, LogLevel, TEXT("%s: %s %s"), *STR_CUR_CLASS_FUNC_LINE, *FString(InString1), *FString(InString2))		               

	/* Log an FString and a Float (One space is automatically added between InString and InFloat)
	*** Example: LOGMSG_F(LogTemp, Warning, "HP:", Health); */
	#define LOGMSG_F(LogCat, LogLevel, InString, InFloat)		UE_LOG(LogCat, LogLevel, TEXT("%s: %s %f"), *STR_CUR_CLASS_FUNC_LINE, *FString(InString), InFloat)			

	/* Log an FString and an Integer (One space is automatically added between InString and InInteger)
	*** Example: LOGMSG_F(LogTemp, Warning, "HitCount:", NumOfHits); */
	#define LOGMSG_I(LogCat, LogLevel, InString, InInteger)		UE_LOG(LogCat, LogLevel, TEXT("%s: %s %d"), *STR_CUR_CLASS_FUNC_LINE, *FString(InString), InInteger)					               

	/* Log a variable FormatString with any variable types
	*** Example: LOGMSG_M(LogTemp, Warning, "HP: %f, Loc: %s", Health, *Location.ToString()); */
	#define LOGMSG_M(LogCat, LogLevel, FormatString, ...)		UE_LOG(LogCat, LogLevel, TEXT("%s: %s"), *STR_CUR_CLASS_FUNC_LINE, *FString::Printf(TEXT(FormatString), ##__VA_ARGS__))

