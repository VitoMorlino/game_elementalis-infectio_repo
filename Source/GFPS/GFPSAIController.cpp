// Fill out your copyright notice in the Description page of Project Settings.

#include "GFPSAIController.h"
#include "GFPSBaseCharacter.h" //This is included indirectly through AICharacter.h, but I don't want to rely on that dependency
#include "GFPSAICharacter.h"
#include "GFPSAIWaypoint.h"
#include "Navigation/CrowdFollowingComponent.h"

//AI Includes
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"


//Constructor
AGFPSAIController::AGFPSAIController()
{
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));

	//Do not log debug messages by default...change this value per-instance, as necessary
	bLogDebugMessages = false;

	/* Match with the Blackboard in Editor */
	BotTypeKeyName = "BotType";
	BotAgroKeyName = "BotAgro";
	ShouldWanderKeyName = "ShouldWander";
	CurrentWaypointKeyName = "CurrentWaypoint";
	LocToGoKeyName = "LocToGo";
	NearbyLocKeyName = "NearbyLoc";
	TargetEnemyKeyName = "TargetEnemy";
}

void AGFPSAIController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);

	AGFPSAICharacter* AIChar = Cast<AGFPSAICharacter>(InPawn);
	if (AIChar)
	{
		//Return if no BehaviorTree was set for the AICharacter
		if (!AIChar->BehaviorTree)
		{
			if (bLogDebugMessages) LOGMSG(NonPlayerCon, Error, "AIChar->BehaviorTree not found");

			return;
		}

		if (AIChar->BehaviorTree->BlackboardAsset)
		{
			if (BlackboardComp)
			{
				BlackboardComp->InitializeBlackboard(*AIChar->BehaviorTree->BlackboardAsset);
			}
			else
			{
				if (bLogDebugMessages) LOGMSG(NonPlayerCon, Error, "BlackboardComp not found");
			}

			/* Make sure the Blackboard has the details of bot we possessed */
			SetBlackboardBotType(AIChar->BotType);
			SetBlackboardBotAgro(AIChar->BotAggressiveness);
			SetBlackboardShouldWander(AIChar->bShouldWander);
		}

		if (BehaviorComp)
		{
			BehaviorComp->StartTree(*AIChar->BehaviorTree);
		}
		else
		{
			if (bLogDebugMessages) LOGMSG(NonPlayerCon, Error, "BehaviorComp not found");
		}
	}
}

void AGFPSAIController::UnPossess()
{
	Super::UnPossess();

	/* Stop any behavior running as we no longer have a pawn to control */
	BehaviorComp->StopTree();
}

void AGFPSAIController::SetBlackboardBotType(EBotBehaviorType NewType)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsEnum(BotTypeKeyName, (uint8)NewType);
	}
}

void AGFPSAIController::SetBlackboardBotAgro(EBotAggressiveness NewAgro)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsEnum(BotAgroKeyName, (uint8)NewAgro);
	}
}

void AGFPSAIController::SetBlackboardShouldWander(bool NewWander)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsBool(ShouldWanderKeyName, NewWander);
	}
}

AGFPSAIWaypoint* AGFPSAIController::GetWaypoint()
{
	if (BlackboardComp)
	{
		return Cast<AGFPSAIWaypoint>(BlackboardComp->GetValueAsObject(CurrentWaypointKeyName));
	}

	return nullptr;
}

void AGFPSAIController::SetWaypoint(AGFPSAIWaypoint* NewWaypoint)
{
	if (BlackboardComp)
	{
		if (bLogDebugMessages) LOGMSG_2(NonPlayerCon, Log, "Setting Waypoint to:",
			NewWaypoint ? NewWaypoint->GetName() : "[nullptr]");

		BlackboardComp->SetValueAsObject(CurrentWaypointKeyName, NewWaypoint);
	}
}

FVector AGFPSAIController::GetLocToGo()
{
	if (BlackboardComp)
	{
		return BlackboardComp->GetValueAsVector(LocToGoKeyName);
	}

	return FVector::ZeroVector;
}

AGFPSBaseCharacter* AGFPSAIController::GetTargetEnemy()
{
	if (BlackboardComp)
	{
		return Cast<AGFPSBaseCharacter>(BlackboardComp->GetValueAsObject(TargetEnemyKeyName));
	}

	return nullptr;
}

void AGFPSAIController::SetTargetEnemy(APawn* NewTarget)
{
	if (BlackboardComp)
	{
		if (bLogDebugMessages) LOGMSG_2(NonPlayerCon, Log, "Setting Target Enemy to:",
			NewTarget ? NewTarget->GetName() : "[nullptr]");

		BlackboardComp->SetValueAsObject(TargetEnemyKeyName, NewTarget);
	}
}

