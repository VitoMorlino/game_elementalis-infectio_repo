// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_GetLocToGo.h"
#include "GFPSAIController.h"
#include "GFPSAIWaypoint.h"
#include "GFPSAICharacter.h"
#include "VitosLogMacros.h"

/* AI Module includes */
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
/* This contains includes all key types like UBlackboardKeyType_Vector used below. */
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"

EBTNodeResult::Type UBTTask_GetLocToGo::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	AGFPSAIController* MyController = Cast<AGFPSAIController>(OwnerComp.GetAIOwner());
	if (!MyController)
	{
		LOGMSG(NonPlayerCon, Error, "AIController not found");
		return EBTNodeResult::Failed;
	}

	bool bLogDebug = MyController->bLogDebugMessages;

	AGFPSAICharacter* AIChar = Cast<AGFPSAICharacter>(MyController->GetCharacter());
	if (!AIChar)
	{
		if (bLogDebug) LOGMSG(NonPlayerCon, Error, "AICharacter not found");
		return EBTNodeResult::Failed;
	}

	AGFPSAIWaypoint* CurrentWaypoint = MyController->GetWaypoint();

	FVector LocToGo = FVector::ZeroVector;

	/* IF (x)?, THEN a, :ELSE: b */
	LocToGo = (CurrentWaypoint) ? CurrentWaypoint->GetActorLocation() : AIChar->InitialLocation;

	/* Assign the LocToGo to the Blackboard */
	if (LocToGo != FVector::ZeroVector)
	{
		/* The selected key should be "LocToGo" in the BehaviorTree setup */
		OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(BlackboardKey.GetSelectedKeyID(), LocToGo);

		if (bLogDebug) LOGMSG(NonPlayerCon, Log, "Successfully set LocToGo");
		return EBTNodeResult::Succeeded;
	}

	if (bLogDebug) LOGMSG(NonPlayerCon, Warning, "Failed to set LocToGo");
	return EBTNodeResult::Failed;
}
