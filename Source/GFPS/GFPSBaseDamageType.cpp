// Fill out your copyright notice in the Description page of Project Settings.

#include "GFPSBaseDamageType.h"

UGFPSBaseDamageType::UGFPSBaseDamageType()
{
	//This damage can kill by default
	bCanKill = true;

	//Default to Melee if no BasicDamageType is set
	BasicDamageType = EBasicDamageType::Melee;

	//Default to None if no ElementalDamageType is set
	ElementalDamageType = EElementalDamageType::None;
}

bool UGFPSBaseDamageType::GetCanKill() const
{
	return bCanKill;
}

EBasicDamageType UGFPSBaseDamageType::GetBasicDamageType() const
{
	return BasicDamageType;
}

EElementalDamageType UGFPSBaseDamageType::GetElementalDamageType() const
{
	return ElementalDamageType;
}

