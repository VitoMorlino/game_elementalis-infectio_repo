// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GFPSGameMode.generated.h"

UCLASS(minimalapi)
class AGFPSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGFPSGameMode();


};

