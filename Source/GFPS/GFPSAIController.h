// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "CustomTypes.h"
#include "GFPSAIController.generated.h"

/**
 * 
 */
UCLASS()
class GFPS_API AGFPSAIController : public AAIController
{
	GENERATED_BODY()
	
public:

	AGFPSAIController();

	/* Enable this per-instance to allow logging of debugging messages for this AIController */
	UPROPERTY(EditAnywhere, Category = "AIController|D E B U G G I N G")
	bool bLogDebugMessages;

private:

	/* Called whenever the controller possesses a character bot */
	virtual void Possess(class APawn* InPawn) override;

	virtual void UnPossess() override;

	class UBehaviorTreeComponent* BehaviorComp;

	class UBlackboardComponent* BlackboardComp;

	UPROPERTY(EditDefaultsOnly, Category = "AIController")
	FName BotTypeKeyName;

	UPROPERTY(EditDefaultsOnly, Category = "AIController")
	FName BotAgroKeyName;

	UPROPERTY(EditDefaultsOnly, Category = "AIController")
	FName ShouldWanderKeyName;

	UPROPERTY(EditDefaultsOnly, Category = "AIController")
	FName CurrentWaypointKeyName;

	UPROPERTY(EditDefaultsOnly, Category = "AIController")
	FName LocToGoKeyName;

	UPROPERTY(EditDefaultsOnly, Category = "AIController")
	FName NearbyLocKeyName;

	UPROPERTY(EditDefaultsOnly, Category = "AIController")
	FName TargetEnemyKeyName;

public:

	UFUNCTION(BlueprintCallable)
	void SetBlackboardBotType(EBotBehaviorType NewType);

	UFUNCTION(BlueprintCallable)
	void SetBlackboardBotAgro(EBotAggressiveness NewAgro);

	UFUNCTION(BlueprintCallable)
	void SetBlackboardShouldWander(bool NewWander);

	UFUNCTION(BlueprintCallable)
	class AGFPSAIWaypoint* GetWaypoint();

	UFUNCTION(BlueprintCallable)
	void SetWaypoint(class AGFPSAIWaypoint* NewWaypoint);

	UFUNCTION(BlueprintCallable)
	FVector GetLocToGo();

	UFUNCTION(BlueprintCallable)
	class AGFPSBaseCharacter* GetTargetEnemy();

	UFUNCTION(BlueprintCallable)
	void SetTargetEnemy(APawn* NewTarget);

	/** Returns BehaviorComp subobject **/
	FORCEINLINE UBehaviorTreeComponent* GetBehaviorComp() const { return BehaviorComp; }


	/** Returns BlackboardComp subobject **/
	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }
	
	
};
