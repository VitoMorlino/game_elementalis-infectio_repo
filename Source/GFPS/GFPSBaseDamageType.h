// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "CustomTypes.h"
#include "GFPSBaseDamageType.generated.h"

/**
 * 
 */
UCLASS()
class GFPS_API UGFPSBaseDamageType : public UDamageType
{
	GENERATED_BODY()

	UGFPSBaseDamageType();

	/** Can this damage type kill?
	*	If false and the final blow is of this data type, the damage will be mitigated */
	UPROPERTY(EditDefaultsOnly)
	bool bCanKill;

	/* The specific type can be checked against resistances */
	UPROPERTY(EditDefaultsOnly)
	EBasicDamageType BasicDamageType;

	/* The ElementalDamageType can be checked against resistances */
	UPROPERTY(EditDefaultsOnly)
	EElementalDamageType ElementalDamageType;

public:

	bool GetCanKill() const;
		//Note: "const" here says this method should not alter member variables

	/** Gets the BasicDamageType of this BaseDamageType
	* @return The BasicDamageType */
	UFUNCTION(BlueprintCallable)
	EBasicDamageType GetBasicDamageType() const;
		//Note: "const" here says this method should not alter member variables

	/** Gets the ElementalDamageType of this BaseDamageType
	* @return The ElementalDamageType */
	UFUNCTION(BlueprintCallable)
	EElementalDamageType GetElementalDamageType() const;
		//Note: "const" here says this method should not alter member variables
	
};
