// Fill out your copyright notice in the Description page of Project Settings.

#include "GFPSCharacterMovementComponent.h"
#include "GFPSBaseCharacter.h"

UGFPSCharacterMovementComponent::UGFPSCharacterMovementComponent()
{

	bOrientRotationToMovement = true; // Face in the direction we are moving...
	RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate (default 0.0f, 720.0f, 0.0f)
	GravityScale = 2.f; // Gravity is multiplied by this amount for the character (default 2.0f)
	AirControl = 0.80f; // How much lateral movement control the character has mid-air (default 0.80f)
	JumpZVelocity = 800.f; // Initial upward velocity to apply per jump (default 1000.0f)
	GroundFriction = 3.f; // Higher values allow faster change in direction (default 3.0f)
	MaxWalkSpeed = 250.f; // (default 600.0f)
	MaxFlySpeed = 250.f; // (default 600.0f)
	MaxSwimSpeed = 250.f; // (default 600.0f)
	MaxWalkSpeedCrouched = 150.f; // (default 300.f)
	MaxCustomMovementSpeed = 250.f; // (default 600.0f)
}

float UGFPSCharacterMovementComponent::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const AGFPSBaseCharacter* CharOwner = Cast<AGFPSBaseCharacter>(PawnOwner);
	if (CharOwner)
	{
		// Speed up while sprinting
		if (CharOwner->IsSprinting())
		{
			MaxSpeed *= CharOwner->GetSprintingSpeedMultiplier();
		}
	}

	return MaxSpeed;
}

void UGFPSCharacterMovementComponent::OnCharacterStuckInGeometry(const FHitResult* Hit)
{
	Super::OnCharacterStuckInGeometry(Hit);

	LOGMSG(LogTemp, Error, "Character is stuck in geometry! *** Please let Vito know how this happened");
	SCREENMSG(5.f, ORANGE, "Character is stuck in geometry! *** Please let Vito know how this happened");
}

void UGFPSCharacterMovementComponent::HandleImpact(
	const FHitResult& Hit, float TimeSlice, const FVector& MoveDelta)
{
	Super::HandleImpact(Hit, TimeSlice, MoveDelta);
}

void UGFPSCharacterMovementComponent::ApplyImpactPhysicsForces(
	const FHitResult& Impact, const FVector& ImpactAcceleration, const FVector& ImpactVelocity)
{
	Super::ApplyImpactPhysicsForces(Impact, ImpactAcceleration, ImpactVelocity);
}

