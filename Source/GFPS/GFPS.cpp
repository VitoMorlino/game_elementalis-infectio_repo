// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "GFPS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GFPS, "GFPS" );

DEFINE_LOG_CATEGORY(BaseChar);

DEFINE_LOG_CATEGORY(PlayerCon);

DEFINE_LOG_CATEGORY(PlayerChar);

DEFINE_LOG_CATEGORY(NonPlayerChar);

DEFINE_LOG_CATEGORY(NonPlayerCon);

DEFINE_LOG_CATEGORY(DmgResist);

DEFINE_LOG_CATEGORY(PickupActor);

DEFINE_LOG_CATEGORY(Obstacle);
