// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GFPSBaseCharacter.h"
#include "CustomTypes.h" //This is included indirectly through BaseCharacter.h, but I don't want to rely on that dependency
#include "GFPSAICharacter.generated.h"

/**
 * 
 */
UCLASS()
class GFPS_API AGFPSAICharacter : public AGFPSBaseCharacter
{
	GENERATED_BODY()

public:

	// Sets default values for this character's properties
	AGFPSAICharacter(const FObjectInitializer& ObjectInitializer);

	//This bool was in the UE4 Example Project, Survival Game, but wasn't referenced anywhere I could find
	//UPROPERTY(BlueprintReadWrite, Category = "Attacking")
	//bool bIsPunching;

protected:

	//BEGIN BaseCharacter Interface

	/* Overridden to make NPC sprint OnSeePawn */
	virtual bool IsSprinting() const override;

	/* Overridden to disallow taking damage if this NPC is Passive */
	virtual float TakeDamage(
		float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	//END BaseCharacter Interface

private:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;


/*****************************************************************************/
/*      AI                                                                   */
/*****************************************************************************/

public:

	/** Should this NPC wander around their waypoint/location? 
	*	Must be true for MaxWanderDistance to apply 
	*	If false, NPC will go to the exact location of the waypoint */
	UPROPERTY(EditAnywhere, Category = "BaseCharacter|AICharacter|AI")
	bool bShouldWander;

	/** Maximum distance to go from waypoint/location when wandering
	*	This only applies if ShouldWander is true */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|AICharacter|AI")
	float MaxWanderDistance;

	/** The bot behavior we want this bot to follow (Idle/Patrol)
	*	Before Runtime: Edit this value per-instance when placed on the map
	*	During Runtime: Use SetBotType() to change this value */
	UPROPERTY(EditAnywhere, Category = "BaseCharacter|AICharacter|AI")
	EBotBehaviorType BotType;

	/** How aggressive we want this bot to be (Aggressive, PassiveAggressive, Passive)
	*	Before Runtime: Edit this value per-instance when placed on the map
	*	During Runtime: Use SetBotAggressiveness() to change this value */
	UPROPERTY(EditAnywhere, Category = "BaseCharacter|AICharacter|AI")
	EBotAggressiveness BotAggressiveness;

	/** The brain of this NPC - makes decisions based on the data we feed it from the Blackboard
	*	Assigned at the Character level (instead of Controller) so we may use different behavior trees while re-using one controller. */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|AICharacter|AI")
	class UBehaviorTree* BehaviorTree;

	/** Change bot type during gameplay
	* @param NewType The new behavior this NPC should follow (Idle/Patrol) */
	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|AICharacter|AI")
	void SetBotType(EBotBehaviorType NewType);

	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|AICharacter|AI")
	void SetBotAggressiveness(EBotAggressiveness NewAgro);

	void SetShouldWander(bool bNewWander);

	FVector InitialLocation;

/*****************************************************************************/
/*      Pawn Sensing                                                         */
/*****************************************************************************/

protected:

	/* Triggered by pawn sensing component when a pawn is spotted */
	UFUNCTION()
	void OnSeePawn(APawn* SensedPawn);

	/* Triggered by pawn sensing component when a noise is heard */
	UFUNCTION()
	void OnHearNoise(APawn* SensedPawn, const FVector& Location, float Volume);

	UFUNCTION(BlueprintNativeEvent)
	void LostTarget();
	void LostTarget_Implementation();

private:

	/* Last time the player was spotted */
	//float LastSeenTime;

	/* Last time the player was heard */
	//float LastHeardTime;

	/* Timer handle to manage continous melee attacks while in range of a player */
	FTimerHandle TimerHandle_SenseTimeOut;

	/** Time-out value to clear the sensed position of the player. 
	* Should be higher than Sense interval in the PawnSense component to never miss sense ticks. */
	UPROPERTY(EditAnywhere, Category = "BaseCharacter|AICharacter|AI")
	float SenseTimeOut;

	/* Resets after sense time-out to avoid unnecessary clearing of target each tick */
	bool bSensedTarget;

	/* The component used by this NPC to 'see' other pawns and 'hear' noises */
	UPROPERTY(VisibleAnywhere, Category = "BaseCharacter|AICharacter|AI")
	class UPawnSensingComponent* PawnSensingComp;

/*****************************************************************************/
/*      Attacking                                                            */
/*****************************************************************************/

protected:

	/* An actor is in melee range */
	UFUNCTION()
	void OnMeleeCompBeginOverlap(
		class UPrimitiveComponent* OverlappedComponent,
		class AActor* OtherActor,
		class UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	/** Attempt to hit the ActorToHit with a Melee Attack
	* @param ActorToHit The actor to hit with a melee strike */
	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|AICharacter|Attacking")
	void PerformMeleeStrikeOn(AActor* ActorToHit);

	/** Deal damage to the Actor that was hit by the MeleeAttack animation
	* @param ActorToHit The actor to hit with a melee strike */
	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|AICharacter|Attacking")
	void DealMeleeDamageTo(AActor* ActorToDmg);

	/* Timer handle to manage continous melee attacks while in range of a player */
	FTimerHandle TimerHandle_MeleeAttack;

	/* Minimum time between melee attacks */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|AICharacter|Attacking")
	float MeleeStrikeCooldown;

	/* Plays the MeleeAnimMontage and SoundAttackMelee */
	void SimulateMeleeStrike();

	void RetriggerMeleeStrike();

	/** How much damage this NPC does per melee hit */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|AICharacter|Attacking")
	float MeleeDamageAmount;

	/* The type of damage to apply on a melee attack */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BaseCharacter|AICharacter|Attacking")
	TSubclassOf<class UDamageType> MeleeDamageType;

	/** Animation Montage to play as this NPC's melee attack */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|AICharacter|Attacking")
	UAnimMontage* MeleeAnimMontage;

	/* An actor overlapping this component is in range for a melee strike */
	UPROPERTY(VisibleAnywhere, Category = "BaseCharacter|AICharacter|Attacking")
	class UCapsuleComponent* MeleeCollisionComp;

	//BEGIN BaseCharacter Interface
	virtual void PlayHit(
		float DamageTaken,
		struct FDamageEvent const& DamageEvent,
		APawn* PawnInstigator,
		AActor* DamageCauser,
		bool bKilled)
			override;
	//END BaseCharacter Interface

private:

	/* Last time we attacked something */
	float LastMeleeAttackTime;

/*****************************************************************************/
/*      Sound                                                                */
/*****************************************************************************/

protected:

	/* Update the vocal loop of the AI (Sound Idle, Patrolling, or Chasing) */
	void UpdateAudioLoop(bool bNewSensedTarget);

	class UAudioComponent* PlayCharacterSound(class USoundCue* CueToPlay);

	/* The sound cue to play when this NPC notices the player */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|AICharacter|Sound")
	class USoundCue* SoundPlayerNoticed;

	/* The sound cue to loop while this NPC is chasing the player */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|AICharacter|Sound")
	class USoundCue* SoundChasing;

	/* The sound cue to loop while this NPC is idle (BotType = Passive) */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|AICharacter|Sound")
	class USoundCue* SoundIdle;

	/* The sound cue to loop while this NPC is patrolling (BotType = Patrol) */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|AICharacter|Sound")
	class USoundCue* SoundPatrolling;

	/* The sound cue to play when this NPC performs a melee attack */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|AICharacter|Sound")
	class USoundCue* SoundAttackMelee;

	/* Plays the Idle, Patrolling, or Chasing sound (The sound of this audio component is set automatically)*/
	UPROPERTY(VisibleAnywhere, Category = "BaseCharacter|AICharacter|Sound")
	class UAudioComponent* AudioLoopComp;

};
