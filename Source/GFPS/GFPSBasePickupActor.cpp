// Fill out your copyright notice in the Description page of Project Settings.

#include "GFPSBasePickupActor.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "GFPSBaseCharacter.h"
#include "VitosLogMacros.h"

//Component Includes
#include "Components/StaticMeshComponent.h"


// Sets default values
AGFPSBasePickupActor::AGFPSBasePickupActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Do not log debug messages by default...change this value per-instance, as necessary
	bLogDebugMessages = false;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->bGenerateOverlapEvents = true;
	RootComponent = MeshComp;

}

// Called when the game starts or when spawned
void AGFPSBasePickupActor::BeginPlay()
{
	Super::BeginPlay();

	InitialTransform = GetTransform();
	OriginalCollisionType = MeshComp->GetCollisionEnabled();

	if (bStartActive)
	{
		RespawnPickup();
	}
	else
	{
		if (bLogDebugMessages) LOGMSG(PickupActor, Log, "bStartActive is false...Hiding this PickupActor");

		MeshComp->SetVisibility(false);
		MeshComp->SetSimulatePhysics(false);
		MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

// Called every frame
void AGFPSBasePickupActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGFPSBasePickupActor::RespawnPickup_Implementation()
{
	if (bLogDebugMessages) LOGMSG(PickupActor, Log, "Attempting to spawn...");

	/* Return if MeshComp == nullptr */
	if (!MeshComp)
	{
		if (bLogDebugMessages) LOGMSG(PickupActor, Error, "MeshComp not found...Failed to spawn");
		return;
	}

	/* If this PickupActor has been moved since BeginPlay(), put it back at its InitialTransform */
	if (!GetTransform().Equals(InitialTransform)) SetActorTransform(InitialTransform);

	bool bCanSpawn = false;

	//If something is in the way of the MeshComp spawning, try to respawn again after SpawnFixDelay
	if (IsSpawnObstructed())
	{
		if (SpawnFixDelay > 0.f)
		{
			/* Start RespawnTimerHandle and call RespawnPickup() when it ends */
			FTimerHandle SpawnFixTimerHandle;
			GetWorldTimerManager().SetTimer(
				SpawnFixTimerHandle, this, &AGFPSBasePickupActor::RespawnPickup, SpawnFixDelay, false);
		}
		else //SpawnFixDelay <= 0 forces spawn even though spawn is obstructed
		{
			if (bLogDebugMessages) LOGMSG_2(PickupActor, Warning, "Forcing spawn even though something is in the way",
				"*** If the player (or something else) got stuck/launched, this may be the cause.");

			bCanSpawn = true;
		}
	}
	else //if spawn is not obstructed
	{
		bCanSpawn = true;
	}

	if (bCanSpawn)
	{
		MeshComp->SetVisibility(true, true);
		MeshComp->SetCollisionEnabled(OriginalCollisionType);

		bIsActive = true;

		if (SpawnSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, SpawnSound, GetActorLocation());
		}

		if (bLogDebugMessages) LOGMSG(PickupActor, Log, "...Successfully spawned");
	}
	else
	{
		if (bLogDebugMessages) LOGMSG(PickupActor, Warning, "...Failed to spawn");
	}
}

bool AGFPSBasePickupActor::IsSpawnObstructed()
{
	if (bLogDebugMessages) LOGMSG(PickupActor, Log, "Checking for obstructions...");

	bool bSpawnObstructed = false;

	if (!MeshComp)
	{
		if (bLogDebugMessages) LOGMSG(PickupActor, Error, "MeshComp not found...nothing to be obstructed");
		return false;
	}

	/* Get the original collision response to pawns, then change it to overlap */
	ECollisionResponse CollisionResponseToPawns = MeshComp->GetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn);
	MeshComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	/** Get the MeshComp's original collision type, then set it to query only so we can
	*	check for overlapping components without affecting physics */
	ECollisionEnabled::Type CollisionType = MeshComp->GetCollisionEnabled();
	MeshComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	/* Create an array and populate it with all components overlapping the MeshComp */
	TArray<UPrimitiveComponent*> OverlappingComps;
	MeshComp->GetOverlappingComponents(OverlappingComps);

	/* Loop through each overlapping component until we find something obstructing the MeshComp */
	for (int32 i = 0; i < OverlappingComps.Num(); i++)
	{
		/* The spawn is obstructed if the overlapping component blocks MeshComp's collision object type */
		if (OverlappingComps[i]->GetCollisionResponseToChannel(MeshComp->GetCollisionObjectType()) == ECollisionResponse::ECR_Block)
		{
			if (bLogDebugMessages) LOGMSG_M(PickupActor, Warning, "Spawn obstructed by Actor [%s]'s component: [%s]",
				*GetNameSafe(OverlappingComps[i]->GetOwner()), *GetNameSafe(OverlappingComps[i]));

			bSpawnObstructed = true;

			break; //Break out of the loop because we already found something obstructing the spawn
		}
	}

	/* Set the collision response to pawns back to what it was originally (before we changed it) */
	MeshComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, CollisionResponseToPawns);

	/* Set MeshComp's collision type back to what it was originally (before we changed it) */
	MeshComp->SetCollisionEnabled(CollisionType);

	if (bSpawnObstructed)
	{
		if (bLogDebugMessages) LOGMSG(PickupActor, Log, "...Spawn is obstructed");
	}
	else
	{
		if (bLogDebugMessages) LOGMSG(PickupActor, Log, "...Spawn is clear of obstructions");
	}

	return bSpawnObstructed;
}

void AGFPSBasePickupActor::Pickup(AGFPSBaseCharacter* CharPickingUp)
{
	if (bIsActive)
	{
		OnPickup(CharPickingUp);
	}
}

void AGFPSBasePickupActor::OnPickup_Implementation(AGFPSBaseCharacter* CharThatPickedUp)
{
	if (bLogDebugMessages) LOGMSG(PickupActor, Log, "Attempting to pick up...");

	bIsActive = false;

	if (MeshComp)
	{
		MeshComp->SetVisibility(false, true);
		MeshComp->SetSimulatePhysics(false); //Just in case SimulatePhysics was set to true from elsewhere
		MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	if (PickupSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, PickupSound, GetActorLocation());
	}

	if (bLogDebugMessages) LOGMSG(PickupActor, Log, "...Successfully picked up");

	if (bAllowRespawnOnPickup)
	{
		/* Start RespawnTimerHandle and call RespawnPickup() when it ends */
		FTimerHandle RespawnTimerHandle;
		GetWorldTimerManager().SetTimer(
			RespawnTimerHandle, this, &AGFPSBasePickupActor::RespawnPickup, RespawnDelayAfterPickup + FMath::RandHelper(RespawnDelayRange), false);

		if (bLogDebugMessages) LOGMSG(PickupActor, Log, "Respawn allowed...Respawning when timer expires...");
	}
	else //if (!bAllowRespawnOnPickup)
	{
		Destroy();

		if (bLogDebugMessages) LOGMSG(PickupActor, Log, "Respawn not allowed...PickupActor Destroyed");
	}
}

